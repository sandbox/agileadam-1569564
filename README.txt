TODO: handle when there is no default flag (which includes when the admin turns this into a SingleFlag type).  When they turn it into a single flag type, the database ought to be purged of all records but 1 per user.  It'd be easier to purge all records for this flag type... should probably warn the admin too!
TODO: remove link to "unflag" a SingleFlag altogether (instead of just re-flagging if
the user tries to unflag their default)
TODO: validate to make sure admin doesn't try to make a SingleFlag global.
TODO: check if more than one record is being returned for get_default() and is_default()
