<?php

/**
 * @file
 * Makes certain flags usable on only one node per user
 * 
 * Lets administrators determine which flags to turn into Single Flag flags.
 * Single Flag flags are usable on only one node at a time. (Think of setting
 * node as a "default"; you will only need one default)
 */

/**
 * Implementation of hook_form_alter().
 *
 * Add a checkbox to the flag settings page for each flag to
 * make the flag type a Single Flag flag
 */
function singleflag_form_alter(&$form, &$form_state, $form_id) {
  $default_value = (singleflag_is_singleflag($form['#flag']->fid)) ? 1 : 0;
  if ($form_id == 'flag_form') {
    $form['flag_form']['singleflag'] = array(
      '#type' => 'checkbox',
      '#title' => 'Make this flag a Single Flag',
      '#description' => t('Check this if you want to allow users to use this flag on only one node'),
      '#default_value' => $default_value,
    );

    // Add an additional submission handler that will process the new checkbox
    $form['#submit'][] = 'singleflag_admin_form_submit';
  }
}

/**
 * Handle the submission of the flag settings form
 * and process the additional Single Flag checkbox
 */
function singleflag_admin_form_submit($form, &$form_state) {
  $singleflag_types = variable_get('singleflag_types', '');
  if ($form_state['values']['singleflag'] == 1) {
    $singleflag_types[$form['#flag']->fid] = 1;
  }
    else {
    $singleflag_types[$form['#flag']->fid] = 0;
  }
  variable_set('singleflag_types', $singleflag_types);
}

/**
 * Implementation of hook_flag().
 *
 * Delete from {flag_content} any other nodes flagged with a Single Flag flag
 * which makes sure only 1 Single Flag flag remains
 */
function singleflag_flag($action, $flag, $content_id, $account) {
  global $user;
  if (singleflag_is_singleflag($flag->fid)) {
    if (!db_result(db_query('SELECT content_id FROM {flag_content} WHERE fid = %d 
      AND uid = %d', $flag->fid, $user->uid))) {
      // Re-flag if user tried unsetting the default flag
      $flag->flag('flag', $content_id);
      drupal_set_message(t('You cannot unflag your default.
          Try setting a different node as your default.'));
    }
    else {
    $result = db_query('DELETE FROM {flag_content}
      WHERE fid = %d AND content_id <> %d AND uid = %d',
      $flag->fid, $content_id, $user->uid);
    }
  }
}

/**
 * Determine if the passed fid is set as a Single Flag
 *
 * @param $fid
 *   The flag id of the flag being checked
 * @return
 *   TRUE if the fid is a Single Flag; False if not
 */
function singleflag_is_singleflag($fid) {
  $singleflag_types = variable_get('singleflag_types', '');
  if ($singleflag_types && array_key_exists($fid, $singleflag_types)) {
    if ($singleflag_types[$fid] == 1) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Determine if the current item has been flagged with a Single Flag flag.
 *
 * @param $flag_name
 *   The name of the flag you want to check. This must be a Single Flag flag.
 * @param $content_id
 *   The content_id of the node (usually) you want to check
 * @return
 *   Returns TRUE if the item has been flagged with the passed Single Flag flag
 */
function singleflag_is_default($flag_name, $content_id) {
  global $user;
  $flag = flag_get_flag($flag_name);
  if (singleflag_is_singleflag($flag->fid)) {
    $result = db_result(db_query('SELECT fid FROM {flag_content}
      WHERE fid = %d AND content_id = %d AND uid = %d', 
      $flag->fid, $content_id, $user->uid));
    if ($result) {
      return TRUE;
    }
  }
  else {
    if (user_access('administer site configuration')) {
      drupal_set_message('Either the flag type is not a Single Flag, or the flag type does not exist.', 'error');
    }
    watchdog('singleflag', 'singleflag_is_default(): You\'re trying to use a non-Single Flag flag, 
      or a flag that does not exist.', array(), WATCHDOG_WARNING);
  }
}

/**
 * Get the single content_id that is flagged with a Single Flag
 * It would be nice to return the loaded content (user, node, taxonomy term, etc.)
 * but there are probably too many issues with this.  Instead, this
 * will return the content_id so the developer can do a node_load() or user_load() (etc.)
 * on their own.
 *
 * @param $flag_name
 *   The name of the Single Flag flag you want to check.
 * @param $fid
 *   The fid of the Single Flag flag you want to check.
 * @return
 *   Returns a content id
 */
function singleflag_get_default_content_id($flag_name = NULL, $fid = NULL) {
  global $user;
  $flag = ($flag_name) ? flag_get_flag($flag_name) : flag_get_flag(NULL, $fid);
  if (singleflag_is_singleflag($flag->fid)) {
    $result = db_result(db_query('SELECT content_id FROM {flag_content} 
      WHERE fid = %d AND uid = %d', $flag->fid, $user->uid));
    if ($result) {
      return $result;
    }
  }
  else {
    if (user_access('administer site configuration')) {
      drupal_set_message('Either the flag type is not a Single Flag, or the flag type does not exist.', 'error');
    }
    watchdog('singleflag', 'singleflag_get_default(): You\'re trying to use a non-Single Flag flag, 
      or a flag that does not exist.', array(), WATCHDOG_WARNING);
  }
}
